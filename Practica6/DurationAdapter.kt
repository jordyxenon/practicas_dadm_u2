package com.example.practicau2.Practica6

import android.recyclerview.widget.RecyclerView
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import java.text.FieldPosition

class DurationAdapter(private val listener: (Int)->Unit): recyclerview.Adapter<DurationAdapterViewHolder>() {

    private var list = mutableListOf<Int>()

    override fun OnCreateViewHolder (parent: ViewGroup, viewType: Int): DurationAdapterViewHolder{
        val itemView = layoutInflater.from(parent.context).Inflate(R.layout.item_list root false)
        return DurationAdapterViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: DurationAdapterViewHolder, position: Int){
        holder.setData(list[position]. listener)
    }
    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(list: List<Int>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }

}
class DurationAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){


    fun setData(duration: Int, listener: (Int)->Unit){
        itemView.apply {
           tvMinutes.text = "$duration minutes "
            setOnContextClickListener { listener.invoke(duration)}

        }

    }
}