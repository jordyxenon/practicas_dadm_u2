         xxxxxg<2p0.package com.example.practicau2.Practica6

import android.recyclerview.widget.RecyclerView
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.example.practicau2.R
import java.text.FieldPosition

class AdapterExample: RecyclerView.Adapter<AdapterExampleViewHolder>() {
     fun setData(pola: POLA){

}
}


    val list = mutableListOf<POLA>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapterExampleViewHolder {
        val itemView = layoutInflater.from(parent.context).inflate(R.layout.item_list, parent, false)
        return  AdapterExampleViewHolder(itemView )
    }

    override fun onBindViewHolder(holder: AdapterExampleViewHolder, position: Int) {
        holder.setData(list[position])
    }

    override fun getItemCount(): Int {
        return list.size
    }

}

class AdapterExampleViewHolder(itemView): RecyclerView.ViewHolder(itemVView) {

}

data class POLA(var name: String, var led: String)


class CitiestimezonesAdapterAdapter(private val listener: (CitiestimezonesAdapterAdapter.CitiesTimezones)->Unit): recyclerview.Adapter<DurationAdapterViewHolder>() {

    private var list = mutableListOf<CitiestimezonesAdapterAdapter.CitiesTimezones>()

    override fun OnCreateViewHolder (parent: ViewGroup, viewType: Int): CitiesTimezonesAdapterViewHolder{
        val itemView = layoutInflater.from(parent.context).Inflate(R.layout.item_list root false)
        return DurationAdapterViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: CitiesTimezonesAdapterViewHolder, position: Int){
        holder.setData(list[position]. listener)
    }
    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(list: List<CitiestimezonesAdapterAdapter.CitiesTimezones>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }

}
class CitiesTimezonesAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){


    fun setData(item: CitiestimezonesAdapterAdapter.CitiesTimezones, listener: (CitiestimezonesAdapterAdapter.CitiesTimezones)->Unit){
        itemView.apply {
            tvCC.text = "$(item.name)/$(item.country"
             tvTz.text = "item.TimeZoneName"
             setOnClickListener(listener.invoke(item)) }

        }

    }
}