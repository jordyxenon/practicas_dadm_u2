package com.example.practicau2.Practica3

import android.content.Intent
import android.content.IntentSender
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.practicau2.R
import kotlinx.android.synthetic.main.activity_practica3.*

class Practica3Activity : AppCompatActivity() {

    var gender = com.example.practicau2.Practica3.Gender.MALE.namee


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica3)

        btnShow.setOnClickListener {
            val bundle = Bundle()
            val intent = Intent(this, P3Activity::class.java)

            if (etName.text.isNotEmpty() && etLastname.text.isNotEmpty()
                    && etAge.text.isNotEmpty() &&
                    etSalary.text.isNotEmpty()
            ) {
                bundle.putString("NAME", etName.text.toString())
                bundle.putString("LASTNAME", etLastname.text.toString())
                bundle.putInt("AGE", etAge.text.toString().toInt())
                bundle.putInt("SALARY", etSalary.text.toString().toInt())
                bundle.putInt("GENDER", gender)


                intent.putExtras(bundle)
                startActivity(intent)

            } else {
                Toast.makeText(this, "Por favor rellene todos los campos", Toast.LENGTH_SHORT).show()
            }
        }


        rgGender.setOnCheckedChangeListener { group, checkedId ->

            when (ckeckedId) {
                R.id.rbMale -> {
                    gender = com.example.practicau2.Practica3.Gender.MALE.namee
                }
                R.id.rbFemale -> {
                    gender = com.example.practicau2.Practica3.Gender.FEMALE.namee
                }
                R.id.rbNB -> {
                    gender = com.example.practicau2.Practica3.Gender.NOT.BINARY.namee
                }
                R.id.rbNI -> {
                    gender = com.example.practicau2.Practica3.Gender.NOT_IDENTYFIED.namee
                }
            }
        }
    }
}