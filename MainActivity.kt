package com.example.practicau2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnPractica1.setOnclickListener {
            val intent = intent(this Practica1Activity ::class.java)
            startActivity(intent)
        }
        btnPractica2.setOnclickListener {
            val intent = intent(this Practica2Activity ::class.java)
            startActivity(intent)
        }
        btnPractic3.setOnclickListener {
            val intent = intent(this Practica3Activity ::class.java)
            startActivity(intent)
        }
        btnPractica4.setOnclickListener {
            val intent = intent(this Practica4Activity ::class.java)
            startActivity(intent)

        }

        btnPractica6.setONclickListener {
            val intent = intent(this Practica6Activity ::class.java)
            startActivity(intent)
        }
    }
}
