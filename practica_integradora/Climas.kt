 package com.example.practicau2.practica_integradora0   var tiempo: ArrayList<ClimaTiempo> = arrayListOf()
    var icon: String = ""
    var summary: String = ""

    init{
        icon = dailyJo.getString("icon")
        summary = dailyJo.getString("summary")
        val dailyJo = dailyJo.getJSONArray("data")
        for(i in 0 until dailyJo.length()){
            val data = dailyJo.getJSONObject(i)
            tiempo.add(ClimaTiempo(data))
        }
    }
    class ClimaTiempo(dailyJo: JSONObject){
        var time: Long =0
        var icon = ""
        var temperatureMin: Double=0.0
        var temperatureMax: Double=0.0

        init{
            time = dailyJo.getLong("time")
            icon = dailyJo.getString("icon")
            temperatureMin = dailyJo.getDouble("temperatureMin")
            temperatureMax = dailyJo.getDouble("temperatureMax")
        }
    }
}