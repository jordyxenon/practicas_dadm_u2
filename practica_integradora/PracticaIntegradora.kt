package com.example.practicau2.practica_integradora

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.practicau2.R
import org.json.JSONObject
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter
import java.io.Writer

class practica_integradora : AppCompatActivity() {
    private val adapter by lazy {
        ClimaAdapter { selectedClima ->

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica7)
        val ciudad = intent.getSerializableExtra("ciudad") as String
        Toast.makeText(this, "" + cuidad, Toast.LENGTH_SHORT).show()
        when (ciudad) {
            "Seoul" -> {

            }
        }

        rvClm.adapter = adapter
        val input = resources.openRawResource(R.raw.weather)
        val writer = StringWriter()
        val buffer = CharArray(1024)
        input.use { input ->
            val reader = BufferedReader(InputStreamReader(input, "UTF-8"))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1) {
                writer.write(buffer, 0, n)
            }
        }
        val jsonObject = JSONObject(writer.toString())
        val tiempoList = Climas(jsonObject)

        adapter.setList(tiempoList.tiempo)
    }
}