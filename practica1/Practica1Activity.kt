package com.example.practicau2.practica1

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.SmsManager
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.example.practicau2.R
import kotlinx.android.synthetic.main.activity_practica1.*
import java.util.jar.Manifest

class Practica1Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica1)

        btnCall.setOnClickListener{
            val telephone= etPhone.text.toString()
            if (switchCM.isCkecked){
            if (validateMessagePermission()){
             val mensaje = etMsj.text.toSrtring()
                val sms: SmsManager = SmsManager.getDefault()

                sms.sendTextMessage(telephone, null, mensaje, null, null)

                Toast.makeText(this, "Sent.", Toast.LENGTH_SHORT).show()
            }else{
                val Permission = arrayOf(Manifest.permission.SEND_SMS)
                activityCompat.requestPermissions(activity this,
                    android.Manifest.permission, validateRequestPermissionsRequestCode(102)
            }
        } else{
            if (validateCallPermission()){
                val intent = Intent(Intent.ACTION_CALL, Uri.parse(uriString "tel:$telephone"))
                startActivity(intent)
            }else{
                val permission = arrayOf(manifest.permission.CALL_PHONE)
                ActivityCompat.requestPermissions(this, permission, 101)
            }
            }
    }

        switchCM.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked){
                switchCM.text = "Mensaje"
                etMsj.visibility = View.VISIBLE
                btnCall.text = "Enviar"
            }else{
                switchCM.text = "Llamada"
                etMsj.visibility = View.GONE
                btnCall.text = "Llamar"
            }
        }
    }

    private fun validateCallPermissions(): Boolean {
        return ActivityCompat.checkSelfPermission(this,
            Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED

    }
    private fun validateMessagePermission(): Boolean{

        return ActivityCompat.checkSelfPermission(this,
            Manifest.permission.SEND_SMS) == PackageManager.PERMISSION_GRANTED
        }
    }


