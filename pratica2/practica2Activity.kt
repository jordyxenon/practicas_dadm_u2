package com.example.practicau2.pratica2

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.practica
import com.example.practicau2.R
import kotlinx.android.synthetic.main.activity_practica2.*

const val DURATION_RESULT = 4000

class Practica2Activity : AppCompatActivity() {


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == RESULT_OK) {
            when (requestCode) {
                DURATION_RESULT -> {
                    if (data != null) {
                        val minutes = data.getIntExtra("DURATION", -1)
                        if (minutes >= 60) {
                            tvSelected.text = "{minutes / 60} horas"
                        } else {
                            tvSelected.text = "$minutes minutes"
                        }
                    }
                }
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_practica2)


        btnSelector.setOnClickListener {
            val intent = Intent(this, DurationSelectorActivity::class.java)
            startActivityForResult(intent, DURATION_RESULT)

        }
    }
}