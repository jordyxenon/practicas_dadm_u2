package com.example.practicau2.pratica2

import android.recyclerview.widget.RecyclerView
import android.view.OrientationEventListener
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.recyclerview.widget.RecyclerView
import com.example.practicau2.R
import java.text.FieldPosition

class DurationAdapter(private val listener: (Int)->Unit):
    RecyclerView.Adapter<DurationAdapterViewHolder>() {

    private var list = mutableListOf<Int>()

    override fun OnCreateViewHolder (parent: ViewGroup, viewType: Int): DurationAdapterViewHolder{
        val itemView =
            layoutInflater.from(parent.context).Inflate(R.layout.item_list, parent, false)
        return DurationAdapterViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: DurationAdapterViewHolder, position: Int){
        holder.setData(list[position], listener)
    }
    override fun getItemCount(): Int {
        return list.size
    }

    fun setList(list: MutableList<Int>){
        this.list.addAll(list)
        notifyDataSetChanged()
    }

}
class DurationAdapterViewHolder(itemView: View): RecyclerView.ViewHolder(itemView){


    fun setData(duration: Int, listener: (Int)->Unit){

        itemView.apply {
          var horas: Int = 0
           if (duration /  60 > 0) {
               horas = duration / 60
               tvMinutes.text = "{horas} horas"
           }else {
               tvMinutes.text= "$duration minutes"
           }
               setOnClickListener{ listener.invoke(duration) }

        }

    }
}