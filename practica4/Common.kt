package com.example.practicau2.practica4

import android.app.Activity
import android.app.AlertDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.Gravity
import android.view.WindowManager
import com.example.practicau2.R

object Common {

    fun customDialog(activity: Activity, listener:  (Boolean) -> Unit): AlertDialog{
        val builder = AlertDialog.Builder(activity)

        val inflater = activity.layoutInflater
        val view = inflater.inflate((R.layout.custom_dialog, null)
        builder.setView(view)
        val dialog = builder.create()

        dialog.window?.attributes?.windowAnimations = R.style.widget_AppCompat_ListPopupWindow
        dialog.requestWindowFeature(window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val wmlp: WindowManager.LayoutParams? = dialog.window?.attributes
        wmlp?.gravity = Gravity.BOTTOM

        view.button.setOnClickListener{
            listener.invoke(true)
            dialog.dismiss()
        }
        view.button2.setOnClickListener{
            listener.invoke(false)
            dialog.dismiss()
        }
        return dialog
    }
}