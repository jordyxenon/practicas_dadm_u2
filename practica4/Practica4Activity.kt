package com.example.practicau2.practica4

import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import kotlinx.android.synthetic.main.activity_practica3.*
import kotlinx.android.synthetic.main.activity_practica4.*
import java.nio.file.attribute.AclEntry
import com.example.practicau2.R as R1

class Practica4Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R1.layout.activity_practica4)


        btnAd.setOnClickListener {
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
            builder.setMessage("Esto es un cuadro de dialogo")
                .setTitle("HOLA")
                .setPositiveButton("SIMON") { dialog, which ->
                    Toast.makeText(, this"Amonos", Toast.LENGTH_SHORT).show()
                }
                        .setNeutralButton("no lo se"){ dialog, which ->
                    Toast.makeText(this, "meh", Toast.LENGTH_SHORT).show()
                }
                .setNegativeButton("NELSON")   { dialog, which ->
                    Toast.makeText(this, "Ihh", Toast.LENGTH_SHORT).show()
                }
            val dialog: AlertDialog = builder.create()
            dialog.show()
        }
       btnAd2.setOnClickListener{
           val colors = arrayOf("Red", "Blue", "Green", "Yellow")
           val builder: AlertDialog.Builder = AlertDialog.Builder(this)
           .setTitle("ESCOGE UN COLOR")
           setItems(colors){ dialog, which ->
               when (which){
                   0 -> Toast.makeText(this, "Red", Toast.LENGTH_SHORT).show()
                   1 -> Toast.makeText(this, "Blue", Toast.LENGTH_SHORT).show()
                   2 -> Toast.makeText(this, "Green", Toast.LENGTH_SHORT).show()
                   3 -> Toast.makeText(this, "Yellow", Toast.LENGTH_SHORT).show()

               }

           }

           val dialog = builder.create()
           dialog.show()

       }
        btnAd3.setOnClickListener {
            val colors = arrayOf("Red", "Blue", "Green", "Yellow")
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                    .setTitle("ESCOGE UN COLOR")
                    .setSingleChoiceItems(colors, 0){ dialog,wich ->
                        Toast.makeText(this, "seleccionaste: ${colors[which]}", Toast.LENGTH_SHORT)
                                .show()
        }
            val dialog = builder.create()
            dialog.show()
    }

        val selectedItems = arrayListOf<Int>()
        btnAd4.setOnClickListener {
            val colors = arrayOf("Red", "Blue", "Green", "Yellow")
            val builder: AlertDialog.Builder = AlertDialog.Builder(this)
                    .setTitle("ESCOGE UN COLOR")
                    .setMultiChoiceItems(colors, null, ) {dialog, which, isChecked ->
                        if  (isChecked) {
                            selectedItems.add(which)
                            Toast.makeText(this, "Selected Items: ${selectedItems.size}",
                                    Toast.LENGTH_SHORT).show()

                        }else if (selectedItems.contains(which)){

                            selectedItems.remove(which)
                            Toast.makeText(this, "Selected Items: ${selectedItems.size}",
                            Toast.LENGTH_SHORT).show()
                        }
                        }
            val dialog = builder = builder.create()
            dialog.show()
        }

        btnAd5.setOnClickListener {
            Common.customDialog(this){ yes ->
                if (yes){
                    Toast.makeText(this,"YES", Toast.LENGTH_SHORT).show()
                }else{
                    Toast.makeText(this, "NEl", Toast.LENGTH_SHORT.show)
            }
            }.show()
        }
}
}